
module "namespace" {
  source   = "git::https://gitlab.com/nolte-collection-v2/modules/tf-modules/tf-k8s-namespace.git//modules/namespace"
  name     = var.namespace
  metadata = var.namespace_metadata
}

locals {
  EXTRA_VALUES = {
  }
}

# https://artifacthub.io/packages/helm/bitnami/contour

resource "helm_release" "release" {
  name       = "contour"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "contour"
  version    = var.chart_version
  namespace  = module.namespace.this.metadata[0].name
  timeout    = 600
  values = [
    yamlencode(local.EXTRA_VALUES),
    yamlencode(var.extra_values)
  ]
}
