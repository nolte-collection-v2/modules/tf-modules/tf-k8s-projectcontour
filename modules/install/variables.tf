variable "namespace" {
  default = "projectcontour"
}

variable "chart_version" {
  default = "2.3.4"
}

variable "extra_values" {
  default = {}
}

variable "namespace_metadata" {
  type = object({
    supporters = string
    solution   = string
  })
  default = {
    supporters = "cluster-admins"
    solution   = "technical"
  }
}
