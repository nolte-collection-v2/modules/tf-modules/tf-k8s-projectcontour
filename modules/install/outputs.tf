output "release" {
  value = helm_release.release
}
output "namespace" {
  value = module.namespace.this
}
output "annotations" {
  value = {
    "kubernetes.io/ingress.class" = "contour"
  }
}
